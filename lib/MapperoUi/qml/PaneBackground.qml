import QtQuick 2.12

Rectangle {
    anchors.fill: parent
    color: "white"
    opacity: 0.8
    radius: 10
}
