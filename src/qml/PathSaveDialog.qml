import QtQuick 2.12
import QtQuick.Dialogs 1.3

FileDialog {
    property var path

    title: "Choose a file name"
    selectExisting: false
    defaultSuffix: "gpx"
    //fileUrl: Qt.formatDate(new Date(), Qt.ISODate) + ".gpx"

    onFolderChanged: { console.log("Folder changed: " + folder) }
    onAccepted: {
        console.log("Dialog accepted: " + fileUrl)
        path.saveFile(fileUrl);
    }
}
