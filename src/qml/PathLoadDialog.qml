import QtQuick 2.12
import QtQuick.Dialogs 1.3

FileDialog {
    title: "Choose a file"
    selectExisting: true
    nameFilters: Qt.platform.os != "android" ?
        [ "Tracks (*.gpx *.kml)" ] : undefined

    onFolderChanged: { console.log("Folder changed: " + folder) }
}
