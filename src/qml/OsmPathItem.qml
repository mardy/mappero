import QtQuick 2.12
import QtQuick.Controls 2.12

Menu {
    id: root

    property var path

    signal openPathRequested()
    signal savePathRequested()

    margins: 20

    Action {
        text: qsTr("Open…")
        onTriggered: root.openPathRequested()
    }
    Action {
        text: qsTr("Save…")
        enabled: !path.empty
        onTriggered: root.savePathRequested()
    }
    Action {
        text: qsTr("Clear path")
        enabled: !path.empty
        onTriggered: path.clear()
    }
}
