import QtQuick 2.12
import QtQuick.Window 2.12
import Mappero 1.0

Window {
    id: mainWindow
    width: 800
    height: 480
    visible: true

    onActiveFocusItemChanged: console.log("Active focus item: " + activeFocusItem)

    Loader {
        id: loader
        anchors.fill: parent
        focus: true
        source: application.firstPage
    }
}
