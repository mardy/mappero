import QtQml 2.12

QtObject {
    id: root

    property alias interval: timer.interval
    property var pathItem: null
    property string filePath

    property date _lastEndTime
    property var _timer: Timer {
        id: timer
        interval: 60 * 1000
        running: true
        repeat: true
        onTriggered: root.save()
    }

    function save() {
        if (!pathItem) {
            console.warn("autosave: Path is null")
            return
        }

        if (pathItem.endTime - _lastEndTime == 0) {
            return
        }

        pathItem.saveFile(filePath)
        _lastEndTime = pathItem.endTime
    }
}
